package com.amerim.fishinggame

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.amerim.fishinggame.databinding.FragmentMainMenuBinding
import java.lang.IllegalArgumentException
import java.time.Year

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [mainMenuFragment.nInsta] factory method to
 * create an instance of this fragment.
 */
class mainMenuFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var pm1: String? = null
    private var pm2: String? = null
    private lateinit var binding : FragmentMainMenuBinding
    private lateinit var navController : NavController
    private lateinit var gedaVidasMoqq: GedaVidasMoqq
    private var gameModel: GameModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val qwertyKeyListenerqw = listOf("a","f","s")
            for (ij in qwertyKeyListenerqw) {
                // Дополнительные действия с элементом коллекции
                println("Item: $ij")
            }

            // Использование условных конструкций для различных сценариев
            val privte="luilouil"
            privte.plus("af")
            privte.equals("fb")
            pm1 = it.getString(ARG_PARAM1)
            pm2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        gedaVidasMoqq = ViewModelProvider(requireActivity()).get(GedaVidasMoqq::class.java)
        gameModel = gedaVidasMoqq.gameLiveModel.value
        binding.buttonPlayLevelGame.setOnClickListener {
            if (gameModel!=null)
            {
                gameModel!!.level = 1
            }
            navController.navigate(R.id.action_mainMenuFragment_to_gameFragment)
        }


        if (gameModel != null)
        {
            if (gameModel!!.level1HS == -1) {
                binding.textEarthHighscoreCount.text = "0"
            } else {
                binding.textEarthHighscoreCount.text = gameModel!!.level1HS.toString()
            }


        }



    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment mainMenuFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun nInsta(param1: String, param2: String) =
            mainMenuFragment().apply {
                val r: IllegalArgumentException
                val e: Observer<Int>
                var x= emptyArray<String>()
                val bt:String="saasf ggpp adsa"
                bt.plus("fafqq")
                println(bt)
                Log.d("eee","qerrrookg dfs")
                x.plus("ggg")
                println(x)
                val y=true
                val n: Context
                var m:MutableCollection<Year>
                Log.d("nnn", y.toString())
                val s= mutableListOf(2,1,3,5)
                for(i in 1..(s.size-1)){
                    val bf = s[i]
                    var j = i-1
                    while (j >= 0 && bf<s[j]){
                        s[j+1] = s[j]
                        j--
                    }
                    s[j+1] = bf
                }
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}