package com.amerim.fishinggame

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.amerim.fishinggame.databinding.FragmentGameBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "pm1"
private const val ARG_PARAM2 = "pm2"

/**
 * A simple [Fragment] subclass.
 * Use the [gFrag.nInsta] factory method to
 * create an instance of this fragment.
 */
class gFrag : Fragment() {
    // TODO: Rename and change types of parameters
    private var pm1: String? = null
    private var pm2: String? = null
    private lateinit var binding : FragmentGameBinding
    private lateinit var navController : NavController
    private lateinit var gedaVidasMoqq: GedaVidasMoqq
    private var gameModel: GameModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val MIN_SUM_VALUE=12
            val maxSumValue=144
            val MIN_ANSWER_VALUE=1
            val sum = Random.nextInt(MIN_SUM_VALUE, maxSumValue + 1)
            val visibleNumber = Random.nextInt(MIN_ANSWER_VALUE, sum)
            val options = HashSet<Int>()
            val items = listOf("Apple", "Banana", "Cherry", "Date", "Fig")
            val vv = Random(System.currentTimeMillis())

            // Выбираем случайный элемент из списка
            val dd = items[vv.nextInt(items.size)]
            val text = """
                Kotlin is a statically-typed programming language.
                It is designed to be fully interoperable with Java.
                Kotlin is concise and expressive.
                Kotlin is used for Android app development.
                """.trimIndent()

            val words = text.split(Regex("\\s+"))
            val wordCountMap = mutableMapOf<String, Int>()
            pm1 = it.getString(ARG_PARAM1)
            pm2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        println("fafafa svjsklvjsdfk")
        var a:Int=12
        var b:Int=34
        var c=a+b
        val m= listOf(1,2,3,4,5)
        val z=m.get(2)
        print(z)
        println("afag bbbf")
        binding = FragmentGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        gedaVidasMoqq = ViewModelProvider(requireActivity()).get(GedaVidasMoqq::class.java)
        gameModel = gedaVidasMoqq.gameLiveModel.value

        if (gameModel!=null)
        {
            binding.viewGame.level = gameModel!!.level
        }

        binding.viewGame.lifeListener.observe(viewLifecycleOwner, Observer {
            binding.textLivesCounter.text = binding.viewGame.pyer.lives.toString()
            if (binding.viewGame.pyer.lives == 0)
            {
                binding.viewGame.isRunning = false
                if (gameModel!=null)
                {
                    gameModel!!.lastScore = binding.viewGame.pyer.coins
                }
                navController.navigate(R.id.action_gameFragment_to_gameOverFragment)
            }
        })

        binding.viewGame.coinListener.observe(viewLifecycleOwner, Observer {
            binding.textScore.text = binding.viewGame.pyer.coins.toString()
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param pm1 Parameter 1.
         * @param pm2 Parameter 2.
         * @return A new instance of fragment gameFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun nInsta(param1: String, param2: String) =
            gFrag().apply {
                println("afasf")
                println("5445")
                var n=12
                println("124")
                println("532")
                Log.d("ajghj",n.toString())
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, pm1)
                    putString(ARG_PARAM2, pm2)
                }
            }
    }
}