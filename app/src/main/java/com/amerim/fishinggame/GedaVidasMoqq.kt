package com.amerim.fishinggame

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GedaVidasMoqq: ViewModel() {
    val gameLiveModel = MutableLiveData<GameModel>()

    init {
        gameLiveModel.value = GameModel(1, -1, -1, -1, 0)
    }
}