package com.amerim.fishinggame

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.amerim.fishinggame.databinding.FragmentGameOverBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "pm1"
private const val ARG_PARAM2 = "pm2"

/**
 * A simple [Fragment] subclass.
 * Use the [gOvFrag.nInsta] factory method to
 * create an instance of this fragment.
 */
class gOvFrag : Fragment() {
    // TODO: Rename and change types of parameters
    private var pm1: String? = null
    private var pm2: String? = null
    private lateinit var binding : FragmentGameOverBinding
    private lateinit var navController : NavController
    private lateinit var gedaVidasMoqq: GedaVidasMoqq
    private var gameModel: GameModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            println("iiii")
            println("tt")
            println("cc")
            println("qq")
            println("ee")
            val m=12
            val g=65
            pm1 = it.getString(ARG_PARAM1)
            pm2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var x=12
        var y=23
        val m= listOf<String>()
        var c=x+y
        print(c.compareTo(1))
        println("vsdsdvv")
        Log.d("sdfs","ff")
        println("qrqr gopbgp[p")
        binding = FragmentGameOverBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        binding.buttonMainMenu.setOnClickListener {
            navController.navigate(R.id.action_gameOverFragment_to_mainMenuFragment)
        }

        gedaVidasMoqq = ViewModelProvider(requireActivity()).get(GedaVidasMoqq::class.java)
        gameModel = gedaVidasMoqq.gameLiveModel.value

        if (gameModel!=null)
        {
            when (gameModel!!.level) {
                1 -> {
                    binding.frameLayout2.background = requireContext().resources.getDrawable(R.drawable.antbackgr, null)
                    if (gameModel!!.lastScore > gameModel!!.level1HS)
                    {
                        gameModel!!.level1HS = gameModel!!.lastScore
                    }
                }

            }
        }

        gedaVidasMoqq.gameLiveModel.observe(viewLifecycleOwner, Observer {
            if (gameModel != null)
            {
                binding.textFinalScore.text = gameModel!!.lastScore.toString()
            }
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment gameOverFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun nInsta(param1: String, param2: String) =
            gOvFrag().apply {
                val vv="asdfasdfa"
                vv.equals("as")
                var asffawew=7
                asffawew+=129
                println(asffawew)
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}